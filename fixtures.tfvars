region = "eu-central-1"


backend_app = {
    name = "hello-world-django-app",
    container_name =  "django_container",
    
    host_port = "80",
    container_port = "80",

    container_cpu = 128
    container_memory = 256
    reserved_cpu = 128
    reserved_memory = 256

    command = ["gunicorn", "-w", "3", "-b", ":80", "django_sample_project.wsgi:application"]
}


frontend_app = {
    name = "react-app"
    container_name =  "react_container"

    host_port = "8001"
    container_port = "80"

    container_cpu = 128
    container_memory = 256
    reserved_cpu = 128
    reserved_memory = 256

    command = []
}