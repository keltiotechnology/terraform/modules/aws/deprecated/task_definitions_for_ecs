/* 
 * Application container definition
*/
module "container" {
  source                      = "cloudposse/ecs-container-definition/aws"
  version                     = "0.57.0"
  
  # container_name is not a string fo image name, but a URI to identifiy the docker image
  container_image             = var.image_url[var.application.name]
  container_name              = var.application.container_name
  port_mappings = [
    {
      containerPort           = var.application.container_port
      hostPort                = var.application.host_port
      protocol                = "tcp"
    }
  ]
    
#  log_configuration = {
#   "logDriver"                 : "awslogs",
#   "options": {
#     "awslogs-create-group"    : true,
#     "awslogs-group"           : "awslogs-${var.application.name}-group",
#     "awslogs-region"          : var.region,
#     "awslogs-stream-prefix"   : "awslogs-${var.application.name}"
#   }
#  }

 command                      = var.application.container_command
}


/* 
 * Task definition for Backend application
*/
resource "aws_ecs_task_definition" "task" {
  family                      = "${var.application.name}_template"
  cpu                         = var.application.reserved_cpu
  memory                      = var.application.reserved_memory
  container_definitions = jsonencode([
    module.container.json_map_object
  ])
  requires_compatibilities    = ["EC2"]
  network_mode                = "bridge"
  execution_role_arn          = aws_iam_role.ecsTaskExecutionRole.arn
}